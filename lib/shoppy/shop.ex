defmodule Shoppy.Shop do
  @moduledoc """
  The Shop context is used to view and place purchases in the store.
  """

  import Ecto.Query, warn: false
  alias Shoppy.Repo

  alias Shoppy.Shop.{Checkout, Order}

  @doc """
  Returns the list of orders.

  ## Examples

      iex> list_orders()
      [%Order{}, ...]

  """
  def list_orders do
    Repo.all(Order)
  end

  @doc """
  Gets a single order.

  Raises if the Order does not exist.

  ## Examples

      iex> get_order!(123)
      %Order{}

  """
  def get_order!(id), do: Repo.get!(Order, id)

  @doc """
  Creates an Order with given list of Products and their quantities. Updates
  inventory accordingly.

  ## Examples

      iex> create_order(%{items: [%{quantity: 2, product_id: Product}]})
      {:ok, %Order{}}

      iex> create_order(%{items: []})
      {:error, ...}

  """
  def create_order(attrs \\ %{}) do
    Ecto.Multi.new()
    |> Ecto.Multi.run(:ordered_products, fn _repo, _prev_values ->
      ordered_products =
        attrs
        |> Map.get(:items, [])
        |> Enum.reduce([], fn
          %{quantity: quantity, product_id: product_id}, items ->
            [%{"quantity" => quantity, "product" => Shoppy.Inventory.get_product!(product_id)} | items]

          _, items -> items
        end)

      {:ok, ordered_products}
    end)
    |> Ecto.Multi.run(:inventory, fn _repo, %{ordered_products: ordered_products} ->
      ordered_products
      |> Enum.reduce_while([], fn ordered_product, products ->
        new_quantity = ordered_product["product"].quantity - ordered_product["quantity"]

        ordered_product["product"]
        |> Shoppy.Inventory.update_product(%{"quantity" => new_quantity})
        |> case do
          {:ok, product} -> {:cont, [product | products]}
          result -> {:halt, result}
        end
      end)
      |> case do
        {:error, _} = error -> error
        products -> {:ok, products}
      end
    end)
    |> Ecto.Multi.run(:order, fn _repo, %{ordered_products: ordered_products} ->
      Checkout.new()
      |> Checkout.put_items(ordered_products)
      |> Checkout.run()
    end)
    |> Repo.transaction()
    |> case do
      {:ok, %{order: order}} ->
        {:ok, order}
      {:error, _failed_operation, failed_value, _changes_so_far} ->
        {:error, failed_value}
    end
  end

  # Resets the Shop context by deleting all placed orders. Returns a tuple
  # where the first element is the number of deleted records.
  def reset do
    Repo.delete_all(Order)
  end
end

defmodule Shoppy.Inventory do
  @moduledoc """
  The Inventory context is used for managing inventory of products within the
  store.
  """

  import Ecto.Query, warn: false
  alias Shoppy.Repo

  alias Shoppy.Inventory.Product

  @doc """
  Returns the list of products.

  ## Examples

      iex> list_products()
      [%Product{}, ...]

  """
  def list_products do
    Repo.all(Product)
  end

  @doc """
  Gets a single product.

  Raises if the Product does not exist.

  ## Examples

      iex> get_product!(123)
      %Product{}

  """
  def get_product!(id), do: Repo.get!(Product, id)

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, ...}

  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, ...}

  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Seeds a set of products into inventory. If products already exist, this
  function will not insert any records.
  """
  def seed() do
    if Repo.exists?(Product) do
      {0, nil}
    else
      current_timestamp = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
      attrs =
        [
          %{name: "Apples", quantity: 500, price: 249, inserted_at: current_timestamp, updated_at: current_timestamp},
          %{name: "Bananas", quantity: 650, price: 325, inserted_at: current_timestamp, updated_at: current_timestamp},
          %{name: "Carrots", quantity: 439, price: 300, inserted_at: current_timestamp, updated_at: current_timestamp},
          %{name: "Dates", quantity: 237, price: 499, inserted_at: current_timestamp, updated_at: current_timestamp},
          %{name: "Eggs", quantity: 120, price: 399, inserted_at: current_timestamp, updated_at: current_timestamp}
        ]

      Repo.insert_all(Product, attrs)
    end
  end

  @doc "Resets inventory to initial values."
  def reset do
    Repo.delete_all(Product)

    seed()
  end
end

defmodule Shoppy.Shop.Checkout do
  @moduledoc false

  alias Shoppy.Repo
  alias Shoppy.Inventory
  alias Shoppy.Shop.Order

  defstruct items: [], total_price: 0

  @doc """
  Returns a new `%Checkout{}` struct with default values for `items` and
  `total_price`.
  """
  def new do
    %__MODULE__{}
  end

  @doc """
  Adds a given list of ordered products as items for an Order. The given list
  of products should consist of `Map`s in the format of `%{"quantity" =>
  pos_integer, "product" => %Shoppy.Inventory.Product{}}`. If a given product
  does not match that format, then it will be ignored.
  """
  def put_items(checkout, []), do: checkout

  def put_items(checkout, [
        %{"product" => %Inventory.Product{} = product, "quantity" => quantity} | products
      ])
      when is_integer(quantity) and quantity > 0 do
    new_item = %{"quantity" => quantity, "name" => product.name, "price_per" => product.price}
    total_price = checkout.total_price + product.price * quantity

    checkout = %{checkout | items: [new_item | checkout.items], total_price: total_price}

    put_items(checkout, products)
  end

  def put_items(checkout, [_ | products]), do: put_items(checkout, products)

  @doc """
  Executes the given `%Checkout{}` by creating a new `Shop.Order` changeset and
  attempting an insert. Returns `{:ok, order}` on success and `{:error,
  changeset}` on failure.
  """
  def run(checkout) do
    purchased_at = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)

    %Order{}
    |> Order.changeset(%{"items" => checkout.items})
    |> Ecto.Changeset.put_change(:purchased_at, purchased_at)
    |> Ecto.Changeset.put_change(:total_price, checkout.total_price)
    |> Repo.insert()
  end
end

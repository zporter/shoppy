defmodule Shoppy.Shop.Order do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  defmodule Item do
    use Ecto.Schema

    embedded_schema do
      field :name, :string
      field :quantity, :integer
      field :price_per, :integer
    end

    @doc false
    def changeset(item, attrs) do
      item
      |> cast(attrs, [:name, :quantity, :price_per])
      |> validate_required([:name, :quantity, :price_per])
      |> validate_number(:quantity, greater_than: 0)
      |> validate_number(:price_per, greater_than_or_equal_to: 0)
    end
  end

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "orders" do
    embeds_many :items, Item
    field :total_price, :integer, default: 0
    field :purchased_at, :naive_datetime

    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [])
    |> cast_embed(:items, required: true)
  end
end

defmodule Shoppy.Repo do
  use Ecto.Repo,
    otp_app: :shoppy,
    adapter: Ecto.Adapters.Postgres
end

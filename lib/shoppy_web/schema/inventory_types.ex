defmodule ShoppyWeb.Schema.InventoryTypes do
  use Absinthe.Schema.Notation

  object :product do
    field :id, :id
    field :name, :string
    field :quantity, :integer
    field :price, :integer
  end
end

defmodule ShoppyWeb.Schema.ShopTypes do
  use Absinthe.Schema.Notation
  import AbsintheErrorPayload.Payload
  import_types AbsintheErrorPayload.ValidationMessageTypes

  alias ShoppyWeb.Resolvers.Shop, as: ShopResolver

  object :order do
    field :id, :id
    field :items, list_of(:order_item)
    field :total_price, :integer
    field :purchased_at, :naive_datetime
  end

  object :order_item do
    field :name, :string
    field :quantity, :integer
    field :price_per, :integer
  end

  input_object :cart_item do
    field :quantity, :integer
    field :product_id, :id
  end

  object :shop_queries do
    @desc "Get all Orders"
    field :orders, list_of(:order) do
      resolve &ShopResolver.list_orders/3
    end
  end

  payload_object(:order_payload, :order)

  object :shop_mutations do
    field :create_order, type: :order_payload, description: "Create an Order" do
      arg :items, non_null(list_of(:cart_item))
      resolve &ShopResolver.create_order/3
      middleware &build_payload/2
    end
  end
end

defmodule ShoppyWeb.Schema do
  use Absinthe.Schema

  import_types Absinthe.Type.Custom
  import_types ShoppyWeb.Schema.InventoryTypes
  import_types ShoppyWeb.Schema.ShopTypes

  alias ShoppyWeb.Resolvers

  query do
    @desc "Get all Products"
    field :products, list_of(:product) do
      resolve &Resolvers.Inventory.list_products/3
    end

    import_fields :shop_queries
  end

  mutation do
    import_fields :shop_mutations

    @desc """
    Resets the store by restocking inventory and deleting orders.
    """
    field :reset, :string do
      resolve &Resolvers.Shop.reset/3
    end
  end
end

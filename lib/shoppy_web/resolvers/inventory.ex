defmodule ShoppyWeb.Resolvers.Inventory do
  alias Shoppy.Inventory

  def list_products(_parent, _args, _resolution) do
    {:ok, Inventory.list_products()}
  end
end

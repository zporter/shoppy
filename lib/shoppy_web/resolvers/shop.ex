defmodule ShoppyWeb.Resolvers.Shop do
  alias Shoppy.{Inventory, Shop}

  def create_order(_parent, args, _resolution) do
    Shop.create_order(args)
  end

  def list_orders(_parent, _args, _resolution) do
    {:ok, Shop.list_orders()}
  end

  def reset(_parent, _args, _resolution) do
    Shop.reset()
    Inventory.reset()

    {:ok, "Successfully reset the shop."}
  end
end

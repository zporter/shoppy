# Shoppy

## Dependencies

This project depends on the [Erlang](https://www.erlang.org/) and [Elixir](https://elixir-lang.org/) programming languages as well as the [PostgreSQL database server](https://www.postgresql.org/).

Erlang and Elixir can be installed with [asdf](https://asdf-vm.com/#/). Simply run `asdf install` from the project root. If you use another tool to manage your Erlang and Elixir versions, reference the `.tool-versions` file to see the versions that were used to build this app.

PostgreSQL can be installed with [Homebrew](https://brew.sh/) on Mac OS using `brew install postgresql`. You can view the version of PostgreSQL by opening the database schema at `priv/repo/structure.sql`.

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser. Here, you'll see a [GraphiQL interface](https://graphql.org/). This interface allows you to execute GraphQL queries and view results from the server.

## Queries

### Products

To view a list of products in inventory, you can use the following query:

```graphql
query {
  products {
    id
    name
    quantity
    price
  }
}
```

### Orders

To view a list of orders placed, use the following query:

```graphql
query {
  orders {
    id
    items {
      name
      quantity
      pricePer
    }
    totalPrice
    purchasedAt
  }
}
```

## Mutations

### Creating an Order

To create an Order, compile a list of products you'd like to order along with the number to order, and issue a query like:

```graphql
mutation CreateOrder {
  createOrder(items: [{productId:"9ff275e6-cb70-4888-a86c-cb279cef9f68",quantity:10}]) {
    result {
      id
      items {
        name
        quantity
        pricePer
      }
      totalPrice
      purchasedAt
    }
    messages {
      message
    }
  }
}
```

The created Order will be returned as the `result` with any error messages returned under `messages`.

To view an example of the error messages, you can issue the following mutation to create an invalid Order:

```graphql
mutation CreateOrder {
  createOrder(items: []) {
    result {
      id
      items {
        name
        quantity
        pricePer
      }
      totalPrice
      purchasedAt
    }
    messages {
      message
    }
  }
}
```

### Resetting the Shop

To reset the shop, which deletes the Orders and resets the products in inventory:

```graphql
mutation {
  reset
}
```

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
  * Absinthe: https://hexdocs.pm/absinthe/overview.html

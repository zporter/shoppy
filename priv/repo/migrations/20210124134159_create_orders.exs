defmodule Shoppy.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :items, :map
      add :total_price, :integer, default: 0
      add :purchased_at, :naive_datetime

      timestamps()
    end

  end
end

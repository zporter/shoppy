defmodule Shoppy.InventoryTest do
  use Shoppy.DataCase

  alias Shoppy.Fixtures
  alias Shoppy.Inventory

  describe "managing products" do
    alias Shoppy.Inventory.Product

    @valid_attrs %{name: "Apples", quantity: 500, price: 250}
    @invalid_attrs %{name: ""}
    @update_attrs %{quantity: 476}

    test "list_products/0 returns all products" do
      product = Fixtures.product()
      assert Inventory.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = Fixtures.product()
      assert Inventory.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Inventory.create_product(@valid_attrs)
      assert product.name == @valid_attrs.name
      assert product.quantity == @valid_attrs.quantity
      assert product.price == @valid_attrs.price
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Inventory.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = Fixtures.product()
      assert {:ok, %Product{} = product} = Inventory.update_product(product, @update_attrs)
      assert product.quantity == @update_attrs.quantity
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = Fixtures.product()
      assert {:error, %Ecto.Changeset{}} = Inventory.update_product(product, @invalid_attrs)
      assert product == Inventory.get_product!(product.id)
    end
  end

  describe "seeding products" do
    test "when products exist" do
      Inventory.create_product(@valid_attrs)

      assert {0, nil} = Inventory.seed()
    end

    test "when no products exist" do
      # There are currently 5 products seeded in the database. Update this
      # number when that changes.
      assert {5, nil} = Inventory.seed()
    end
  end

  describe "resetting inventory" do
    import Ecto.Query

    alias Shoppy.Repo

    test "deletes and re-seeds products" do
      # Insert a couple of products
      %{id: product_1_id} = Fixtures.product()
      %{id: product_2_id} = Fixtures.product()

      assert {5, _} = Inventory.reset()

      refute Repo.exists?(from p in Inventory.Product, where: p.id in [^product_1_id, ^product_2_id])
    end
  end
end

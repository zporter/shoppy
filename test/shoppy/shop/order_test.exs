defmodule Shoppy.Shop.OrderTest do
  use Shoppy.DataCase, async: true

  alias Shoppy.Shop.Order

  describe "changing orders" do
    test "requiring items" do
      changeset = Order.changeset(%Order{}, %{})

      assert "can't be blank" in errors_on(changeset).items
    end
  end

  describe "changing order items" do
    test "required fields" do
      ~w[name quantity price_per]
      |> Enum.each(fn field ->
        changeset = Order.Item.changeset(%Order.Item{}, %{field => ""})
        messages =
          changeset
          |> errors_on()
          |> Map.get(String.to_existing_atom(field), [])

        assert "can't be blank" in messages
      end)
    end

    test "quantity must be a valid number" do
      changeset = Order.Item.changeset(%Order.Item{}, %{"quantity" => 0})

      assert "must be greater than 0" in errors_on(changeset).quantity

      changeset = Order.Item.changeset(%Order.Item{}, %{"quantity" => "NaN"})

      assert "is invalid" in errors_on(changeset).quantity
    end

    test "price_per must be a valid number" do
      changeset = Order.Item.changeset(%Order.Item{}, %{"price_per" => -200})

      assert "must be greater than or equal to 0" in errors_on(changeset).price_per

      changeset = Order.Item.changeset(%Order.Item{}, %{"price_per" => "NaN"})

      assert "is invalid" in errors_on(changeset).price_per
    end
  end
end

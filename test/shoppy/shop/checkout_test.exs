defmodule Shoppy.Shop.CheckoutTest do
  use Shoppy.DataCase, async: true

  alias Shoppy.Shop.Checkout
  alias Shoppy.Fixtures

  test "`new/0` returns a Checkout struct with defaults" do
    assert %Checkout{items: [], total_price: 0} = Checkout.new()
  end

  describe "adding items" do
    test "a negative quantity is ignored" do
      checkout = Checkout.new()

      items = [
        %{"quantity" => -2, "product" => Fixtures.product()}
      ]

      assert %Checkout{items: []} = Checkout.put_items(checkout, items)
    end

    test "a missing product" do
      checkout = Checkout.new()

      items = [
        %{"quantity" => 4, "product" => nil}
      ]

      assert %Checkout{items: []} = Checkout.put_items(checkout, items)
    end

    test "adding multiple items" do
      apples = Fixtures.product(%{name: "Apples"})
      bananas = Fixtures.product(%{name: "Bananas"})
      checkout = Checkout.new()

      items = [
        %{"quantity" => 2, "product" => apples},
        %{"quantity" => 5, "product" => bananas}
      ]

      assert %Checkout{items: items} = Checkout.put_items(checkout, items)
      assert %{"quantity" => 2, "name" => apples.name, "price_per" => apples.price} in items
      assert %{"quantity" => 5, "name" => bananas.name, "price_per" => bananas.price} in items
    end
  end

  describe "running a checkout" do
    test "an invalid checkout" do
      assert {:error, changeset} = Checkout.run(Checkout.new())

      assert "can't be blank" in errors_on(changeset).items
    end

    test "a valid checkout" do
      apples = Fixtures.product(%{name: "Apples", price: 250})
      bananas = Fixtures.product(%{name: "Bananas", price: 350})

      items = [
        %{"quantity" => 2, "product" => apples},
        %{"quantity" => 5, "product" => bananas}
      ]

      checkout = Checkout.new() |> Checkout.put_items(items)

      assert {:ok, order} = Checkout.run(checkout)

      assert length(order.items) == 2
      assert order.purchased_at
      # 500 (apples) + 1750 (bananas)
      assert order.total_price == 2250
    end
  end
end

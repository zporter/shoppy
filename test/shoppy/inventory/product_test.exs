defmodule Shoppy.Inventory.ProductTest do
  use Shoppy.DataCase, async: true

  alias Shoppy.Inventory.Product

  @valid_product %Product{
    name: "Apples",
    price: 350,
    quantity: 500
  }

  describe "changing products" do
    test "validating required fields" do
      ~w[name price]
      |> Enum.each(fn field ->
        changeset = Product.changeset(@valid_product, %{field => ""})
        messages =
          changeset
          |> errors_on()
          |> Map.get(String.to_existing_atom(field), [])

        assert "can't be blank" in messages
      end)
    end

    test "validating price as a number" do
      changeset = Product.changeset(@valid_product, %{"price" => -1})

      assert "must be greater than or equal to 0" in errors_on(changeset).price

      changeset = Product.changeset(@valid_product, %{"price" => "NaN"})

      assert "is invalid" in errors_on(changeset).price
    end

    test "validating quantity as a number" do
      changeset = Product.changeset(@valid_product, %{"quantity" => -1})

      assert "must be greater than or equal to 0" in errors_on(changeset).quantity

      changeset = Product.changeset(@valid_product, %{"quantity" => "NaN"})

      assert "is invalid" in errors_on(changeset).quantity
    end
  end
end

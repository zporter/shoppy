defmodule Shoppy.ShopTest do
  use Shoppy.DataCase, async: true

  alias Shoppy.Shop
  alias Shoppy.Fixtures

  describe "orders" do
    alias Shoppy.Shop.Order

    test "list_orders/0 returns all orders" do
      order = Fixtures.order()
      assert Shop.list_orders() == [order]
    end

    test "get_order!/1 returns the order with given id" do
      order = Fixtures.order()
      assert Shop.get_order!(order.id) == order
    end

    test "create_order/1 with valid data creates an order and updates inventory" do
      apples = Fixtures.product(%{name: "Apples", quantity: 15})
      bananas = Fixtures.product(%{name: "Bananas", quantity: 10})

      attrs = %{
        items: [
          %{quantity: 3, product_id: apples.id},
          %{quantity: 8, product_id: bananas.id}
        ]
      }

      assert {:ok, %Order{} = order} = Shop.create_order(attrs)

      item_attrs = Enum.map(order.items, &Map.take(&1, [:name, :price_per, :quantity]))
      assert %{name: apples.name, price_per: apples.price, quantity: 3} in item_attrs
      assert %{name: bananas.name, price_per: bananas.price, quantity: 8} in item_attrs

      assert %{quantity: 12} = Shoppy.Inventory.get_product!(apples.id)
      assert %{quantity: 2} = Shoppy.Inventory.get_product!(bananas.id)
    end

    test "create_order/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shop.create_order(%{items: []})
    end
  end

  test "resetting the shop" do
    Fixtures.order()
    Fixtures.order()

    assert {2, _} = Shop.reset()
    refute Repo.exists?(Shop.Order)
  end
end

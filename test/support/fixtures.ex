defmodule Shoppy.Fixtures do
  alias Shoppy.Inventory

  def product(attrs \\ %{}) do
    valid_attrs = %{name: "Apples", quantity: 500, price: 250}

    {:ok, product} =
      attrs
      |> Enum.into(valid_attrs)
      |> Inventory.create_product()

    product
  end

  alias Shoppy.Shop

  def order(attrs \\ %{}) do
    valid_attrs =
      %{
        items: [
          %{quantity: 1, product_id: product().id},
          %{quantity: 4, product_id: product(%{name: "Bananas"}).id}
        ]
      }

    {:ok, order} =
      attrs
      |> Enum.into(valid_attrs)
      |> Shop.create_order()

    order
  end
end

defmodule ShoppyWeb.Schema.ShopTypesTest do
  use ShoppyWeb.ConnCase, async: true

  alias Shoppy.Fixtures

  test "query: orders", %{conn: conn} do
    order = Fixtures.order()
    order_2 = Fixtures.order()

    query =
      """
      query {
        orders {
          id
        }
      }
      """

    conn =
      post(conn, "/", %{"query" => query, "variables" => %{}})

    assert json_response(conn, 200) == %{
      "data" => %{
        "orders" => [
          %{"id" => order.id},
          %{"id" => order_2.id}
        ]
      }
    }
  end
end

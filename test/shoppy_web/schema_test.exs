defmodule ShoppyWeb.SchemaTest do
  use ShoppyWeb.ConnCase, async: true

  alias Shoppy.Fixtures

  test "query: products", %{conn: conn} do
    bananas = Fixtures.product(%{name: "Bananas", quantity: 123, price: 350})
    carrots = Fixtures.product(%{name: "Carrots", quantity: 34, price: 120})

    query =
      """
      query {
        products {
          id
          name
          quantity
          price
        }
      }
      """

    conn =
      post(conn, "/", %{"query" => query, "variables" => %{}})

    assert json_response(conn, 200) == %{
      "data" => %{
        "products" => [
          %{"id" => bananas.id, "name" => bananas.name, "quantity" => bananas.quantity, "price" => bananas.price},
          %{"id" => carrots.id, "name" => carrots.name, "quantity" => carrots.quantity, "price" => carrots.price}
        ]
      }
    }
  end

  test "mutation: reset", %{conn: conn} do
    query =
      """
      mutation {
        reset
      }
      """

    conn =
      post(conn, "/", %{"query" => query, "variables" => %{}})

    assert json_response(conn, 200) == %{
      "data" => %{
        "reset" => "Successfully reset the shop."
      }
    }
  end
end
